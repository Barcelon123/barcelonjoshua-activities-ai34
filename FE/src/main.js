import Vue from 'vue'
import App from '../src/components/pages/dashboard.vue'

Vue.config.productionTip = false

import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'


new Vue({
  render: h => h(App),
}).$mount('#app')
