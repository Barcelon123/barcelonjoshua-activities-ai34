<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/9bdf77c2d9.js" crossorigin="anonymous"></script>
    <title>Recommender System</title>
  </head>
  <style>

</style>
  <body>
 
  <nav class="navbar navbar-expand-lg navbar-light  sticky-top" style="background-color: #e3f2fd;">
  <a class="navbar-brand" href="#"><img src="img/logo.png" alt="Logo" style="width:88px;">&nbsp  CTE- Reviewer Center </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
 
  <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
    <ul class="navbar-nav ">
      <li class="nav-item ">
      <a class="nav-link" href="records.php"><i class="fas fa-home"></i>&nbsp Dashboard</a>
      </li>
      &nbsp
      <li class="nav-item">
        <a class="nav-link" href="enroll_list.php"><i class="fas fa-list-alt"></i>&nbsp Enrollees</a>
      </li>
     
      &nbsp
      <li class="nav-item" <?php if(@$_GET['q']==1) echo'class="active"'; ?>>
        <a class="nav-link" href="dash.php?q=1"><i class="fas fa-users"></i>&nbsp User</a>
      </li>
      &nbsp
      <li class="nav-item dropdown <?php if(@$_GET['q']==4 || @$_GET['q']==5) echo'active"'; ?>">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="far fa-file-alt"></i>&nbsp Quiz 
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="dash.php?q=4"><i class="fas fa-plus"></i>&nbsp Add Quiz</a>
          <a class="dropdown-item" href="dash.php?q=5"><i class="fas fa-minus"></i>&nbsp Remove Quiz</a>
          
        </div>
      </li>
      &nbsp
      <li class="nav-item" <?php if(@$_GET['q']==2) echo'class="active"'; ?>>
        <a class="nav-link" href="dash.php?q=2"><i class="fas fa-poll"></i>&nbsp Ranking</a>
      </li>
      &nbsp
      <li class="nav-item">
        <a class="nav-link" href="logout.php"><i class="fas fa-sign-out-alt"></i>&nbsp Logout</a>
      </li>
    </ul>
    
  </div>
</nav>

   
  </body>
</html>
