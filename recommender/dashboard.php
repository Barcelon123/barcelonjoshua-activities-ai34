<?php
include('home.php');

?>

<?php 

include 'db.php';

session_start();

error_reporting(0);

if (isset($_SESSION['username'])) {
    header("Location: home_user.php");
}

if (isset($_POST['submit'])) {
	$email = $_POST['email'];
	$password = md5($_POST['password']);

	$sql = "SELECT * FROM users WHERE email='$email' AND password='$password'";
	$result = mysqli_query($con, $sql);
	if ($result->num_rows > 0) {
		$row = mysqli_fetch_assoc($result);
		$_SESSION['username'] = $row['username'];
		header("Location: home_user.php");
	} else {
		echo "<script>alert('Woops! Email or Password is Wrong.')</script>";
	}
}

?>
<!DOCTYPE html>
<html>
  <head>
   
    <title>Recommender System</title>
    
    <link rel="stylesheet" href="css1/dashboard_style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  </head>
  <style>
  
  .schedule{
    position: absolute;
  left: 70%;
  padding-top: 8%;

    
  }
  </style>
  <body>

<div class="schedule">
<form action="" method="POST">
  <div class="form-group">
  <center><h3><i class="fas fa-user"></i>&nbsp Login User</h3></center>
  <br>
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" name="email" class="form-control"  placeholder="Enter email" value="<?php echo $email; ?>" required>
   
  </div>
 
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password"  name="password" class="form-control"  placeholder="Password" value="<?php echo $_POST['password']; ?>" required>
  </div>
 
  <center>
  <button name="submit" class="btn btn-outline-primary">Login here</button>&nbsp &nbsp &nbsp 
  </center>
  <br>
  <p class="login-register-text">Don't have an account? <a href="register.php">Register Here</a>.</p>
</form>
</div>

    <!--image slider start-->
   <br>
    <h3>&nbsp &nbsp <i class="fas fa-bullhorn"></i> &nbsp Announcement:</h3>
    <div class="slider">
      <div class="slides">
        <!--radio buttons start-->
        <input type="radio" name="radio-btn" id="radio1">
        <input type="radio" name="radio-btn" id="radio2">
        <input type="radio" name="radio-btn" id="radio3">
        <input type="radio" name="radio-btn" id="radio4">
        <!--radio buttons end-->
        <!--slide images start-->
        <div class="slide first">
          <img src="img/1.jpg" alt="">
        </div>
        <div class="slide">
          <img src="img/5.png" alt="">
        </div>
        <div class="slide">
          <img src="img/6.png" alt="">
        </div>
        <div class="slide">
          <img src="img/2.png" alt="">
        </div>
        <!--slide images end-->
        <!--automatic navigation start-->
        <div class="navigation-auto">
          <div class="auto-btn1"></div>
          <div class="auto-btn2"></div>
          <div class="auto-btn3"></div>
          <div class="auto-btn4"></div>
        </div>
        <!--automatic navigation end-->
      </div>
      <!--manual navigation start-->
      <div class="navigation-manual">
        <label for="radio1" class="manual-btn"></label>
        <label for="radio2" class="manual-btn"></label>
        <label for="radio3" class="manual-btn"></label>
        <label for="radio4" class="manual-btn"></label>
      </div>
      <!--manual navigation end-->
    </div>
    <!--image slider end-->
    
    <br>
    <div>
    <?php
include('footer.php');
?>
    </div>
    <script type="text/javascript">
    var counter = 1;
    setInterval(function(){
      document.getElementById('radio' + counter).checked = true;
      counter++;
      if(counter > 4){
        counter = 1;
      }
    }, 5000);
    </script>


  </body>
</html>
      