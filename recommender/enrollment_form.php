<?php
include('template.php');
?>


<!DOCTYPE html>
<html>
<head>
	<title>Recommender System</title>

    

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <script src="https://kit.fontawesome.com/9bdf77c2d9.js" crossorigin="anonymous"></script>
</head>

<body>
    <div class="container">
    <br><br>
    <h1><b><i class="fas fa-clipboard-list"></i>&nbsp Enroll Now! </b></h1>
    <br><br>
	<form class="needs-validation" action="add.php" method="POST" enctype="multipart/form-data"  novalidate>
	
  <div class="form-row">
    <div class="col-md-6 mb-3">
      <label for="validationCustom01">First name</label>
      <input type="text" class="form-control" name="fname" id="validationCustom01" placeholder="First name"  required>
      <div class="invalid-feedback">
	  Please choose a firstname.
      </div>
    </div>
    <div class="col-md-6 mb-3">
      <label for="validationCustom02">Last name</label>
      <input type="text" class="form-control" name="lname" id="validationCustom02" placeholder="Last name"  required>
      <div class="invalid-feedback">
	  Please choose a lastname.
      </div>
    </div>
	</div>
	<div class="form-row">
	<div class="col-md-6 mb-3">
      <label for="validationCustom02">Middle name</label>
      <input type="text" class="form-control" name="mname" id="validationCustom02" placeholder="Middle name"  required>
      <div class="invalid-feedback">
	  Please choose a middle name.
      </div>
    </div>
	<div class="col-md-3 mb-3">
    <label for="validationCustom05">Gender</label>
    <select class="form-control" name="gender" id="validationCustom05">
	<option value="" selected>Choose...</option>
      <option value="Male">Male</option>
      <option value="Female">Female</option>
      <option value="Other">Other</option>
      
    </select>
	</div>
  <div class="col-md-3 mb-3">
      <label for="validationCustom02">Contact Number (Globe or TM Only)</label>
      <input type="text" class="form-control" name="contact" id="validationCustom02" placeholder="Contact"  required>
      <div class="invalid-feedback">
	  Please input contact.
      </div>
    </div>
</div>
  <div class="form-row">
  
	<div class="col-md-6 mb-3">
      <label for="validationCustomUsername">Email</label>
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text" id="inputGroupPrepend">@</span>
        </div>
        <input type="text" class="form-control" name="email"id="validationCustomUsername" placeholder="Email" aria-describedby="inputGroupPrepend" required>
        <div class="invalid-feedback">
          Please input a email.
        </div>
      </div>
  </div>
  <div class="col-md-6 mb-3">
      <label for="validationCustom02">Student ID: (If LNU Graduate)</label>
      <input type="text" class="form-control" name="student_id" id="validationCustom02" placeholder="Student ID"  required>
      <div class="invalid-feedback">
	  Please input your school.
      </div>
    </div>
  </div>
  <div class="form-row">
  
  <div class="col-md-6 mb-3">
    <label for="validationCustom05">Degree</label>
    <select class="form-control" name="degree" id="validationCustom05">
	<option  value="" selected>Choose...</option>
      <option value="BEEd">BEEd</option>
      <option value="BSed">BSed</option>
      <option value="BEEd (Supplemental)">BEEd (Supplemental)</option>
	  <option value="BSed (TCP)">BSed (TCP)</option>
      
    </select>
	</div>
    <div class="col-md-6 mb-3">
      <label for="validationCustom03">School Graduate</label>
      <input type="text" class="form-control" id="validationCustom03"name="school_grad" placeholder="School Graduate" required>
      <div class="invalid-feedback">
        Please provide a school graduate.
      </div>
    </div>
    <div class="col-md-6 mb-3">
      <label for="validationCustom04">Year Graduate</label>
      <input type="text" class="form-control" id="validationCustom04" name="year_grad" placeholder="Year Garduate" required>
      <div class="invalid-feedback">
        Please provide a year graduate.
      </div>
    </div>
	<div class="col-md-6 mb-3">
    <label for="validationCustom05">Academic Awards</label>
    <select class="form-control" name="academic_award" id="validationCustom05">
	<option  value="" selected>Choose...</option>
      <option value="Cumlaude">Cumlaude</option>
      <option value="Magna Cumlaude">Magna Cumlaude</option>
      <option value="Summa Cumlaude">Summa Cumlaude</option>
	  <option value="None">None</option>
      
    </select>
	</div>
	
	<div class="col-md-6 mb-3">
    <label for="validationCustom05">First Taker</label>
    <select class="form-control" name="first_take" id="validationCustom05">
	<option   value="" selected>Choose...</option>
      <option  value="Yes">Yes</option>
      <option  value="No">No</option>
     
      
    </select>
	</div>
	<div class="col-md-6 mb-3">
    <label for="validationCustom05">Majorship</label>
    <select class="form-control" name="majorship" id="validationCustom05">
	<option   value="" selected>Choose...</option>
      <option value="English">English</option>
      <option value="Filipino">Filipino</option>
      <option value="Mathemathics">Mathemathics</option>
	  <option value="Social Science">Social Science</option>
	  <option value="Biological Science">Biological Science</option>
	  <option value="Physical Science">Physical Science</option>
	  <option value="MAPEH">MAPEH</option>
	  <option value="TLE">TLE</option>
	  <option value="Fishery Arts">Fishery Arts</option>
      
    </select>
	</div>
  
  <br><br><br><br>

  
<label><b> Mode of Payment:</b><p> 1. Personal Transaction at LNU Cashier's Office - Present QR Code at LNU Gate
    (Note: Go first to LNU CTE Office for the Registration Form to be presented at LNU Cashier)</p>

<p>2. Palawan Express Padala with details below:
          Name: Ruby Mae H. Peñaranda
          Mobile Num: 0967-253-0502
          Purpose of Transaction: LET Review Registration Fee</p>

<p>3. Gcash 
     (Note: Add 2% of the Total Amount of Payment for the Cash Out Service Fee)
     Send To: 09672530502
     Amount: Payment  + 2% Service Fee = Total Amount
     Message: 
        Name of Reviewee: _________________</p>

<p>Send picture of payment receipt to ctereview2021@gmail.com for confirmation.</p></label>

<div class="col-md-4 mb-3">
    <label for="validationCustom05">Payment Method</label>
    <select class="form-control" name="pay" id="validationCustom05">
	<option  value="" selected>Choose...</option>
      <option value="Personal Transaction at LNU Cashier's Office"> Personal Transaction at LNU Cashier's Office</option>
      <option value="Palawan">Palawan</option>
	  <option value="Gcash">Gcash</option>
     
      
    </select>
	</div>
	<div class="form-group">
    <label for="exampleFormControlFile1">Picture of Signature Electronic signature (E-sign)</label>
    <input type="file" class="form-control-file" name="image" id="exampleFormControlFile1">
  </div>
  <div class="col-md-6 mb-3">
      <label for="validationCustom05">Facebook Profile Name: (for Groupchat)</label>
      <input type="text" class="form-control" name="fb_name" id="validationCustom05" placeholder="Facebook Profile Name" required>
      <div class="invalid-feedback">
        Please provide a Facebook Profile Name.
      </div>
    </div>
  </div>
  
  
	
	
  <div class="form-group">
    <div class="form-check">
      <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
      <label class="form-check-label" for="invalidCheck">
        Agree to terms and conditions
      </label>
      <div class="invalid-feedback">
        You must agree before submitting.
      </div>
    </div>
  </div>
  <button class="btn btn-primary" value="Submit" name="submit" type="submit">Submit form</button>
</form>


        </div>
        <br>
    <div>
    <?php
include('footer.php');
?>
    </div>
</body>
<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>