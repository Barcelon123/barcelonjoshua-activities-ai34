<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/9bdf77c2d9.js" crossorigin="anonymous"></script>
    <title>Recommender System</title>
  </head>
  <style>

</style>
  <body>
 
  <nav class="navbar navbar-expand-lg navbar-light  sticky-top" style="background-color: #e3f2fd;">
  <a class="navbar-brand" href="#"><img src="img/logo.png" alt="Logo" style="width:88px;">&nbsp  CTE- Reviewer Center </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
 
  <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
    <ul class="navbar-nav ">
      <li class="nav-item ">
      <a class="nav-link" href="home_user.php"><i class="fas fa-home"></i>&nbsp Home</a>
      </li>
      
      &nbsp
      
      <li class="nav-item" >
        <a class="nav-link" href="enrollment_form.php"><i class="fas fa-calendar-alt"></i>&nbsp Enrollment</a>
      </li>
      &nbsp
      <li class="nav-item"  <?php if(@$_GET['q']==1) echo'class="active"'; ?>>
        <a class="nav-link" href="account.php?q=1"><i class="fas fa-list-alt"></i>&nbsp Pre-Test</a>
      </li>
    
      &nbsp
      <li class="nav-item" <?php if(@$_GET['q']==2) echo'class="active"'; ?>>
        <a class="nav-link" href="account.php?q=2"><i class="fas fa-history"></i>&nbsp History</a>
      </li>
      &nbsp
      <li class="nav-item"  <?php if(@$_GET['q']==3) echo'class="active"'; ?>>
        <a class="nav-link" href="account.php?q=3"><i class="fas fa-poll"></i>&nbsp Ranking</a>
      </li>
      &nbsp
      <li class="nav-item">
        <a class="nav-link" href="logout.php"><i class="fas fa-sign-out-alt"></i>&nbsp Logout</a>
      </li>
    </ul>
    
  </div>
</nav>

   
  </body>
</html>
