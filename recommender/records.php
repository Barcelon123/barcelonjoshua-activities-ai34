<?php 
include('template_admin.php');
//index.php

require('db.php');
$query = "SELECT * FROM enrollment";
$result = mysqli_query($con, $query);
$chart_data = '';
while($row = mysqli_fetch_array($result))
{
 $chart_data .= "{ year_grad:'".$row["year_grad"]."', first_take:".$row["first_take"].", majorship:".$row["majorship"]."}, ";
}
$chart_data = substr($chart_data, 0, -2);

$total_enroll=mysqli_num_rows($result);
?>

<!DOCTYPE html>
<html>
 <head>
  <title>Recommender System</title>
  
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
  <script src="https://kit.fontawesome.com/9bdf77c2d9.js" crossorigin="anonymous"></script>
 </head>
<style>
.wrapper1{
  height: 200px;
  width: 300px;
  background-color: #e9f1f3;
  margin: 73px;
  text-align:center;
  border: 1px solid white;
  box-shadow: 2px 2px 10px gray;
  float:left;
}
.wrapper2{
  height: 200px;
  width: 300px;
  background-color: #e9f1f3;
  margin: 73px;
  text-align:center;
  border: 1px solid white;
  box-shadow: 2px 2px 10px gray;
  float:left;
}
.wrapper3{
  height: 200px;
  width: 300px;
  background-color: #e9f1f3;
  margin: 73px;
  text-align:center;
  border: 1px solid white;
  box-shadow: 2px 2px 10px gray;
  float:left;
}

h1{
  background-color: #1bc5f0;
  color:white;
  border-bottom: 2px solid white;
  font: size 15em;
}
h2{
  font: size 50em;
  margin top: 50%;
}
h1,h2{
  padding: 20px;
  margin:0px;
}
</style>
 <body>
 <br><br>
 <h3>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<i class="fas fa-chart-line"></i>&nbspDashboard</h3>
  <div class="wrapper1">
      <h1> <i class="fas fa-users"></i>&nbsp Enrollee </h1>
      <h2><?php echo $total_enroll;?></h2>
 
    </div> 
  
  <div class="wrapper2">
      <h1><i class="fas fa-check"></i> &nbsp Passed </h1>
      <h2>10<h2>
 
  </div>
 
  <div class="wrapper3">
      <h1><i class="fas fa-times"></i>&nbsp Failed </h1>
      <h2>35</h2>
 
  </div>
  
 <br><br>
 <div style="margin-left:8%" >
 <div class="container" style="width:900px">
   
   <h3 align="center">Enrollees</h3>   
   <br /><br />
   <div id="chart"></div>
  </div>
  </div>
  <br>
    <div>
    <?php
include('footer.php');
?>
    </div>
 </body>
</html>

<script>
Morris.Line({
 element : 'chart',
 data:[<?php echo $chart_data; ?>],
 xkey:'year_grad',
 ykeys:['majorship'],
 labels:['majorship', 'first_take'],
 hideHover:'auto',
 stacked:true
});
</script>
