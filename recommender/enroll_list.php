<?php
include('db.php');

?>
 <?php
include('template_admin.php');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Recommender System</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	
</head>
<body>
<br><br>	
	<div class="container" style="width: 900px,margin:auto;">
	<br>
	
	<h1><b><i class="fas fa-list-alt"></i> &nbsp List Of Enrollee</b></h1>
	
 
  <hr>
		<table  class="table table-bordered table-striped table-hover" id="myTable">
		<thead>
			<tr>
			   <th class="text-center" scope="col">S.L</th>
				<th class="text-center" scope="col">School ID</th>
				<th class="text-center" scope="col">Name</th>
				<th class="text-center" scope="col">Degree</th>
				<th class="text-center" scope="col">Email</th>
				
				
			</tr>
		</thead>
			<?php

        	$get_data = "SELECT * FROM enrollment order by 1 desc";
        	$run_data = mysqli_query($con,$get_data);
			$i = 0;
        	while($row = mysqli_fetch_array($run_data))
        	{
				$sl = ++$i;
				$id = $row['id'];
				$student_id = $row['student_id'];
				$fname = $row['fname'];
				$lname = $row['lname'];
				$degree = $row['degree'];
				$email = $row['email'];
				

        		$image = $row['image'];

        		echo "

				<tr>
				<td class='text-center'>$sl</td>
				<td class='text-left'>$student_id</td>
				<td class='text-left'>$fname   $lname</td>
				<td class='text-left'>$degree</td>
				<td class='text-left'>$email</td>
				
			
				
				
			</tr>


        		";
        	}

        	?>

			
			
		</table>
		<form method="post" action="export.php">
     <input type="submit" name="export" class="btn btn-success" value="Export Data" />
    </form>
	</div>
	<br>
    <div>
    <?php
include('footer.php');
?>
    </div>
<script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script>
    $(document).ready(function () {
      $('#myTable').DataTable();

    });
  </script>

</body>
</html>