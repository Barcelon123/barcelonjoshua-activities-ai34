<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>footer</title>
    <link rel="stylesheet" href="css1/style1.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    
       
        <div class="footer-bottom">
        <h3>Get connected with us on social network: </h3>
           
            <ul class="socials">
                <li><a href="#"><i class="fa fa-facebook"></i>&nbsp WWW.FACEBOOK/LNUCTE.COM</a></li>
                <li><a href="#"><i class="fas fa-envelope"></i>&nbsp LNU.CTE.R8@GMAIL.COM</a></li>
                <li><a href="#"><i class="fas fa-phone-square-alt"></i>&nbsp (+63) 916 383 7417 </a></li>
                <li><a href="#"><i class="fas fa-map-marker-alt"></i>&nbsp LEYTE NORMAL UNIVERSITY </a></li>
            </ul>
            <p>copyright &copy;2021 CTE-Reviewer Center</p>
        </div>
   
</body>
</html>