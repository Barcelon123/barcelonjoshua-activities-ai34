-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 02, 2021 at 01:34 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `recommender_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `email`, `password`) VALUES
(1, 'abc123@gmail.com', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `admin1`
--

CREATE TABLE `admin1` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin1`
--

INSERT INTO `admin1` (`id`, `username`, `email`, `password`) VALUES
(1, 'Joshua', 'abc@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b');

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE `answer` (
  `qid` text NOT NULL,
  `ansid` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `answer`
--

INSERT INTO `answer` (`qid`, `ansid`) VALUES
('55892169bf6a7', '55892169d2efc'),
('5589216a3646e', '5589216a48722'),
('558922117fcef', '5589221195248'),
('55892211e44d5', '55892211f1fa7'),
('558922894c453', '558922895ea0a'),
('558922899ccaa', '55892289aa7cf'),
('558923538f48d', '558923539a46c'),
('55892353f05c4', '55892354051be'),
('60e267ef627b1', '60e267ef62f59'),
('60e267ef64348', '60e267ef6486e'),
('60e267ef66076', '60e267ef66416'),
('60e3c9fe82250', '60e3c9fe83eb2'),
('60e3ca368a0e7', '60e3ca368c6d9'),
('60e645b3c377d', '60e645b3c584a'),
('60e647963e064', '60e6479645db2'),
('60e64d9441684', '60e64d944226a');

-- --------------------------------------------------------

--
-- Table structure for table `enrollment`
--

CREATE TABLE `enrollment` (
  `id` int(11) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `mname` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contact` int(255) NOT NULL,
  `student_id` int(255) NOT NULL,
  `degree` varchar(255) NOT NULL,
  `school_grad` varchar(255) NOT NULL,
  `year_grad` int(255) NOT NULL,
  `academic_award` varchar(255) NOT NULL,
  `first_take` varchar(255) NOT NULL,
  `majorship` varchar(255) NOT NULL,
  `pay` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `fb_name` varchar(255) NOT NULL,
  `uploaded` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `enrollment`
--

INSERT INTO `enrollment` (`id`, `fname`, `lname`, `mname`, `gender`, `email`, `contact`, `student_id`, `degree`, `school_grad`, `year_grad`, `academic_award`, `first_take`, `majorship`, `pay`, `image`, `fb_name`, `uploaded`) VALUES
(24, 'Barcelon', '21', 'abcg@gmail.com', 'Male', '0091131241', 0, 0, '', '', 0, '', '', '', '', '', '', ''),
(25, 'winston', '23', 'abc12@gmail.com', 'Male', '1241412', 0, 0, '', '', 0, '', '', '', '', '', '', ''),
(27, 'Bengson ', '24', 'abcg@gmail.com', 'Male', '1241412', 0, 0, '', '', 0, '', '', '', '', '', '', ''),
(31, 'sdkj', 'sfh', 'sfh', 'Choose...', 'abcde@gmail.com', 0, 35515, 'Choose...', 'agda', 235, 'Choose...', 'Choose...', 'Choose...', 'weteeeeeeeeeeeeeeeeeeeeee', '2021-07-03 11:37:47', '', ''),
(32, 'sdkj', 'sfh', 'sfh', 'Male', 'abc@gmail.com', 0, 35515, 'BSed', 'agda', 235, 'Cumlaude', 'Yes', 'Fishery Arts', 'weteeeeeeeeeeeeeeeeeeeeee', '2021-07-03 11:38:20', '', ''),
(33, 'sdkj', 'sfh', 'sfh', 'Female', 'abcde@gmail.com', 0, 35515, 'BEEd', 'agda', 235, 'Cumlaude', 'Yes', 'TLE', 'Palawan', '197885144_2896105443982209_2128394832185284376_n.jpg', 'weteeeeeeeeeeeeeeeeeeeeee', '2021-07-03 11:53:59'),
(34, 'Joshua', 'Barcelon', 'Andriano', 'Female', 'abcg@gmail.com', 91131241, 35515, 'BSed', 'agda', 235, 'Cumlaude', 'Yes', 'MAPEH', 'Palawan', '197885144_2896105443982209_2128394832185284376_n.jpg', 'weteeeeeeeeeeeeeeeeeeeeee', '2021-07-03 13:41:13'),
(36, 'Joshua', 'Barcelon', 'sfh', 'Male', 'abc@gmail.com', 91131241, 35515, 'BSed', 'agda', 0, 'Magna Cumlaude', 'Yes', 'TLE', 'Palawan', 'WIN_20210612_17_41_10_Pro.jpg', 'weteeeeeeeeeeeeeeeeeeeeee', '2021-07-04 16:30:17'),
(37, 'Joshua', 'Barcelon', 'Andriano', 'Male', 'abc12@gmail.com', 1241412, 35515, 'BSed (TCP)', 'lnu', 2010, 'Magna Cumlaude', 'Yes', 'TLE', 'Palawan', '', 'MarkJustin', '2021-07-07 16:10:42'),
(38, 'Joshua', 'Barcelon', 'Andriano', 'Male', 'abc12@gmail.com', 1241412, 35515, 'BSed (TCP)', 'lnu', 2010, 'Magna Cumlaude', 'Yes', 'TLE', 'Palawan', '', 'MarkJustin', '2021-07-07 16:11:05'),
(39, 'Joshua', 'Barcelon', 'Andriano', 'Female', 'abc12@gmail.com', 1241412, 1802945, 'BSed', 'lnu', 2010, 'Cumlaude', 'Yes', 'MAPEH', 'Palawan', '71042155_2260506017392106_5380282742602727424_n.jpg', 'MarkJustin', '2021-07-07 16:11:54'),
(40, 'Joshua', 'Barcelon', 'Andriano', 'Female', 'abc12@gmail.com', 1241412, 1802945, 'BSed', 'lnu', 2010, 'Cumlaude', 'Yes', 'MAPEH', 'Palawan', '71042155_2260506017392106_5380282742602727424_n.jpg', 'MarkJustin', '2021-07-07 16:13:02'),
(41, 'Mark Justin', 'Bengson', 'p', 'Male', 'abcde2@gmail.com', 91131241, 35515, 'BEEd', 'lnu', 2010, 'Cumlaude', 'Yes', 'TLE', 'Palawan', '71042155_2260506017392106_5380282742602727424_n.jpg', 'MarkJustin', '2021-07-07 16:15:06');

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `email` varchar(50) NOT NULL,
  `eid` text NOT NULL,
  `score` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `sahi` int(11) NOT NULL,
  `wrong` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`email`, `eid`, `score`, `level`, `sahi`, `wrong`, `date`) VALUES
('sunnygkp10@gmail.com', '558921841f1ec', 4, 2, 2, 0, '2015-06-23 09:31:26'),
('sunnygkp10@gmail.com', '558920ff906b8', 4, 2, 2, 0, '2015-06-23 13:32:09'),
('avantika420@gmail.com', '558921841f1ec', 4, 2, 2, 0, '2015-06-23 14:33:04'),
('avantika420@gmail.com', '5589222f16b93', 4, 2, 2, 0, '2015-06-23 14:49:39'),
('mi5@hollywood.com', '5589222f16b93', 4, 2, 2, 0, '2015-06-23 15:12:56'),
('nik1@gmail.com', '558921841f1ec', 1, 2, 1, 1, '2015-06-23 16:11:50'),
('sunnygkp10@gmail.com', '5589222f16b93', 1, 2, 1, 1, '2015-06-24 03:22:38'),
('abc@gmail.com', '60e2679fac2ad', -6, 3, 0, 3, '2021-07-06 02:25:11'),
('abc12@gmail.com', '60e2679fac2ad', -6, 3, 0, 3, '2021-07-06 02:54:28'),
('bdabc@gmail.com', '60e2679fac2ad', -1, 3, 1, 2, '2021-07-06 02:59:14'),
('abc@gmail.com', '60e3c9f336955', 1, 1, 1, 0, '2021-07-06 03:12:03'),
('12345abc@gmail.com', '60e3ca2f838be', 1, 1, 1, 0, '2021-07-06 07:11:08'),
('12345abc@gmail.com', '60e3c9f336955', -1, 1, 0, 1, '2021-07-06 07:11:20'),
('abcasfdaf@gmail.com', '5589222f16b93', -2, 2, 0, 2, '2021-07-08 00:23:00'),
('abcasfdaf@gmail.com', '60e645a879868', 1, 1, 1, 0, '2021-07-08 00:25:07'),
('abcsacd@gmail.com', '60e645a879868', 1, 1, 1, 0, '2021-07-08 00:26:51'),
('abcasfafd@gmail.com', '60e647538f305', -1, 1, 0, 1, '2021-07-08 00:34:04'),
('abcasfafd@gmail.com', '60e645a879868', 1, 1, 1, 0, '2021-07-08 00:39:23'),
('asfasfabcd@gmail.com', '60e64d893b260', -1, 1, 0, 1, '2021-07-08 01:30:20'),
('asfasfabcd@gmail.com', '60e647538f305', 1, 1, 1, 0, '2021-07-08 01:30:41'),
('asfasfabcd@gmail.com', '60e645a879868', -1, 1, 0, 1, '2021-07-08 01:31:06'),
('abcd@gmail.com', '60e64d893b260', -1, 1, 0, 1, '2021-07-28 06:11:29');

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `qid` varchar(50) NOT NULL,
  `option` varchar(5000) NOT NULL,
  `optionid` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`qid`, `option`, `optionid`) VALUES
('55892169bf6a7', 'usermod', '55892169d2efc'),
('55892169bf6a7', 'useradd', '55892169d2f05'),
('55892169bf6a7', 'useralter', '55892169d2f09'),
('55892169bf6a7', 'groupmod', '55892169d2f0c'),
('5589216a3646e', '751', '5589216a48713'),
('5589216a3646e', '752', '5589216a4871a'),
('5589216a3646e', '754', '5589216a4871f'),
('5589216a3646e', '755', '5589216a48722'),
('558922117fcef', 'echo', '5589221195248'),
('558922117fcef', 'print', '558922119525a'),
('558922117fcef', 'printf', '5589221195265'),
('558922117fcef', 'cout', '5589221195270'),
('55892211e44d5', 'int a', '55892211f1f97'),
('55892211e44d5', '$a', '55892211f1fa7'),
('55892211e44d5', 'long int a', '55892211f1fb4'),
('55892211e44d5', 'int a$', '55892211f1fbd'),
('558922894c453', 'cin>>a;', '558922895ea0a'),
('558922894c453', 'cin<<a;', '558922895ea26'),
('558922894c453', 'cout>>a;', '558922895ea34'),
('558922894c453', 'cout<a;', '558922895ea41'),
('558922899ccaa', 'cout', '55892289aa7cf'),
('558922899ccaa', 'cin', '55892289aa7df'),
('558922899ccaa', 'print', '55892289aa7eb'),
('558922899ccaa', 'printf', '55892289aa7f5'),
('558923538f48d', '255.0.0.0', '558923539a46c'),
('558923538f48d', '255.255.255.0', '558923539a480'),
('558923538f48d', '255.255.0.0', '558923539a48b'),
('558923538f48d', 'none of these', '558923539a495'),
('55892353f05c4', '192.168.1.100', '5589235405192'),
('55892353f05c4', '172.168.16.2', '55892354051a3'),
('55892353f05c4', '10.0.0.0.1', '55892354051b4'),
('55892353f05c4', '11.11.11.11', '55892354051be'),
('60e267ef627b1', 'as', '60e267ef62f59'),
('60e267ef627b1', 'asf', '60e267ef62f5d'),
('60e267ef627b1', 'asf', '60e267ef62f5e'),
('60e267ef627b1', 'asf', '60e267ef62f5f'),
('60e267ef64348', 'sdg', '60e267ef64866'),
('60e267ef64348', 'f', '60e267ef6486d'),
('60e267ef64348', 'xh', '60e267ef6486e'),
('60e267ef64348', 'hhhh', '60e267ef6486f'),
('60e267ef66076', 'd', '60e267ef66413'),
('60e267ef66076', 'd', '60e267ef66416'),
('60e267ef66076', 'd', '60e267ef66417'),
('60e267ef66076', 'd', '60e267ef66418'),
('60e3c9fe82250', 'as', '60e3c9fe83eab'),
('60e3c9fe82250', 'asf', '60e3c9fe83eb2'),
('60e3c9fe82250', 'asf', '60e3c9fe83eb3'),
('60e3c9fe82250', 'asf', '60e3c9fe83eb4'),
('60e3ca368a0e7', '1', '60e3ca368c6d9'),
('60e3ca368a0e7', '1', '60e3ca368c6e2'),
('60e3ca368a0e7', '1', '60e3ca368c6e4'),
('60e3ca368a0e7', '1', '60e3ca368c6e5'),
('60e645b3c377d', 'sa', '60e645b3c583f'),
('60e645b3c377d', 'as', '60e645b3c5847'),
('60e645b3c377d', 'as', '60e645b3c5849'),
('60e645b3c377d', 'v', '60e645b3c584a'),
('60e647963e064', 'aef', '60e6479645da3'),
('60e647963e064', 'ef', '60e6479645db1'),
('60e647963e064', 'sef', '60e6479645db2'),
('60e647963e064', 'sef', '60e6479645db4'),
('60e64d9441684', 'sa', '60e64d944225f'),
('60e64d9441684', 'asd', '60e64d944226a'),
('60e64d9441684', 's', '60e64d944226c'),
('60e64d9441684', 'asd', '60e64d944226e');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `eid` text NOT NULL,
  `qid` text NOT NULL,
  `qns` text NOT NULL,
  `choice` int(10) NOT NULL,
  `sn` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`eid`, `qid`, `qns`, `choice`, `sn`) VALUES
('558920ff906b8', '55892169bf6a7', 'what is command for changing user information??', 4, 1),
('558920ff906b8', '5589216a3646e', 'what is permission for view only for other??', 4, 2),
('558921841f1ec', '558922117fcef', 'what is command for print in php??', 4, 1),
('558921841f1ec', '55892211e44d5', 'which is a variable of php??', 4, 2),
('5589222f16b93', '558922894c453', 'what is correct statement in c++??', 4, 1),
('5589222f16b93', '558922899ccaa', 'which command is use for print the output in c++?', 4, 2),
('558922ec03021', '558923538f48d', 'what is correct mask for A class IP???', 4, 1),
('558922ec03021', '55892353f05c4', 'which is not a private IP??', 4, 2),
('60e2679fac2ad', '60e267ef627b1', 'what?', 4, 1),
('60e2679fac2ad', '60e267ef64348', 'asf?', 4, 2),
('60e2679fac2ad', '60e267ef66076', 'adg', 4, 3),
('60e3c9f336955', '60e3c9fe82250', 'asfasf', 4, 1),
('60e3ca2f838be', '60e3ca368a0e7', 'qwr', 4, 1),
('60e645a879868', '60e645b3c377d', 'asfdasf', 4, 1),
('60e647538f305', '60e647963e064', 'ef', 4, 1),
('60e64d893b260', '60e64d9441684', 'ad', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE `quiz` (
  `eid` text NOT NULL,
  `title` varchar(100) NOT NULL,
  `sahi` int(11) NOT NULL,
  `wrong` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `time` bigint(20) NOT NULL,
  `intro` text NOT NULL,
  `tag` varchar(100) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quiz`
--

INSERT INTO `quiz` (`eid`, `title`, `sahi`, `wrong`, `total`, `time`, `intro`, `tag`, `date`) VALUES
('558920ff906b8', 'Mathematics', 2, 1, 2, 5, '', 'linux', '2021-07-07 06:49:02'),
('558921841f1ec', 'English', 2, 1, 2, 5, '', 'PHP', '2021-07-07 06:49:24'),
('5589222f16b93', 'Science', 2, 1, 2, 5, '', 'c++', '2021-07-07 06:49:37'),
('60e645a879868', 'Biology', 1, 1, 1, 1, 'sfgfsh', '1', '2021-07-08 00:24:08'),
('60e647538f305', 'Physics', 1, 1, 1, 1, '1', '1', '2021-07-08 00:31:15'),
('60e64d893b260', 'Science 2', 1, 1, 1, 1, 'saf', '1', '2021-07-08 00:57:45');

-- --------------------------------------------------------

--
-- Table structure for table `rank`
--

CREATE TABLE `rank` (
  `email` varchar(50) NOT NULL,
  `score` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rank`
--

INSERT INTO `rank` (`email`, `score`, `time`) VALUES
('abc@gmail.com', -8, '2021-07-06 02:25:11'),
('abc12@gmail.com', -10, '2021-07-06 02:57:28'),
('bdabc@gmail.com', -6, '2021-07-06 02:59:52'),
('12345abc@gmail.com', 0, '2021-07-06 07:11:20'),
('sdfdsabcd@gmail.com', 0, '2021-07-07 06:58:24'),
('abcdasvavav@gmail.com', -1, '2021-07-07 23:48:10'),
('abcsacd@gmail.com', 1, '2021-07-08 00:26:51'),
('asfasfabcd@gmail.com', -1, '2021-07-08 01:31:06'),
('abcd@gmail.com', -1, '2021-07-28 06:11:29');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `name` varchar(50) NOT NULL,
  `gender` varchar(5) NOT NULL,
  `college` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mob` bigint(20) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`name`, `gender`, `college`, `email`, `mob`, `password`) VALUES
('Joshua Barcelon', 'F', 'lnu', '12345abc@gmail.com', 124, 'd41d8cd98f00b204e9800998ecf8427e'),
('Joshua Barcelon', 'M', 'lnu', 'abc123@gmail.com', 24, '827ccb0eea8a706c4c34a16891f84e7b'),
('Joshua Barcelon', 'M', 'lnu', 'abc12@gmail.com', 525, '827ccb0eea8a706c4c34a16891f84e7b'),
('Joshua Barcelon12', 'F', 'lnu', 'abc@gmail.com', 5, 'd8578edf8458ce06fbc5bb76a58c5ca4'),
('Abac', 'M', 'evsu', 'abcasfafd@gmail.com', 234, 'd41d8cd98f00b204e9800998ecf8427e'),
('Winston', 'M', 'lnu', 'abcasfdaf@gmail.com', 124, 'd41d8cd98f00b204e9800998ecf8427e'),
('Joshua Barcelon', 'M', 'lnu', 'abcd1234@gmail.com', 124, 'd41d8cd98f00b204e9800998ecf8427e'),
('Joshua Barcelon', 'M', 'lnu', 'abcd@gmail.com', 13, 'd41d8cd98f00b204e9800998ecf8427e'),
('Joshua Barcelon', 'M', 'lnu', 'abcdasvavav@gmail.com', 124, 'd41d8cd98f00b204e9800998ecf8427e'),
('Joshua Barcelon', 'M', 'lnu', 'abcde2@gmail.com', 124, 'd8578edf8458ce06fbc5bb76a58c5ca4'),
('Joshua Barcelon', 'M', 'lnu', 'abcg@gmail.com', 213, '827ccb0eea8a706c4c34a16891f84e7b'),
('Winston', 'M', 'evsu', 'abcsacd@gmail.com', 12, 'd41d8cd98f00b204e9800998ecf8427e'),
('Joshua Barcelon', 'M', 'lnu', 'asfasfabcd@gmail.com', 124124, 'd41d8cd98f00b204e9800998ecf8427e'),
('Bengson ', 'M', 'lnu', 'bdabc@gmail.com', 12414, '827ccb0eea8a706c4c34a16891f84e7b'),
('Joshua Barcelon', 'M', 'lnu', 'sdfdsabcd@gmail.com', 1, 'd41d8cd98f00b204e9800998ecf8427e');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`) VALUES
(2, 'wawa', 'abcde@gmail.com', 'c20ad4d76fe97759aa27a0c99bff6710'),
(3, 'Joshua', 'abc12@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(4, 'Joshua', 'abcde2@gmail.com', '962012d09b8170d912f0669f6d7d9d07'),
(5, 'banny', 'abcde1223@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(6, 'vanvans', 'vanvans@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(7, 'banny', 'wawa@gmail.com', '202cb962ac59075b964b07152d234b70'),
(8, 'Pia', 'pia@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(9, 'bengson', 'bengson@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(11, 'Barcelon', 'abcd@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(12, 'winston', 'abcg@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(13, 'Bengson', 'abcdcfg@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(14, 'Raphael Abac2', 'nikoabac814@gmail.com', '202cb962ac59075b964b07152d234b70'),
(18, 'wawa', 'abc13@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(19, 'Joshua12345', 'abc123456@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(20, 'Joshua', '123456@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(21, 'Joshua12345678', '1abc@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(22, 'Joshua123456781113513515', '123abc@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(23, 'Joshua', '12345abc@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(24, 'Joshua', 'abc@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(25, 'Winston', '123456abc@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(26, 'Winston', 'asabc@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(27, 'Joshua', 'abc1231421435@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(28, 'Joshua', '0123456abc@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(29, 'bengson', 'cdabc@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `admin1`
--
ALTER TABLE `admin1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enrollment`
--
ALTER TABLE `enrollment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin1`
--
ALTER TABLE `admin1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `enrollment`
--
ALTER TABLE `enrollment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
