<?php
include('home.php');
?>

<?php 

include 'db.php';

error_reporting(0);

session_start();

if (isset($_SESSION['username'])) {
    header("Location: dashboard.php");
}

if (isset($_POST['submit'])) {
	$username = $_POST['username'];
	$email = $_POST['email'];
	$password = md5($_POST['password']);
	$cpassword = md5($_POST['cpassword']);

	if ($password == $cpassword) {
		$sql = "SELECT * FROM users WHERE email='$email'";
		$result = mysqli_query($con, $sql);
		if (!$result->num_rows > 0) {
			$sql = "INSERT INTO users (username, email, password)
					VALUES ('$username', '$email', '$password')";
			$result = mysqli_query($con, $sql);
			if ($result) {
				echo "<script>alert('Wow! User Registration Completed.')</script>";
				$username = "";
				$email = "";
				$_POST['password'] = "";
				$_POST['cpassword'] = "";
			} else {
				echo "<script>alert('Woops! Something Wrong Went.')</script>";
			}
		} else {
			echo "<script>alert('Woops! Email Already Exists.')</script>";
		}
		
	} else {
		echo "<script>alert('Password Not Matched.')</script>";
	}
}

?>

<!DOCTYPE html>
<html>
  <head>
   
    <title>Recommender System</title>
    
    <link rel="stylesheet" href="css1/dashboard_style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  </head>
  <style>
  
  .schedule{
    position: absolute;
  left: 70%;
  padding-top: 2%;

    
  }
  </style>
  <body>

<div class="schedule">
<form action="" method="POST">
<div class="container">
  <div class="form-group">
  <center><h3><i class="fas fa-users"></i>&nbsp Register User </h3></center>
  <br>
  
    <label for="exampleInputEmail1">Username</label>
    <input type="text" name="username" class="form-control"  placeholder="Enter Username" value="<?php echo $username; ?>" required>
    </div>
  
  
  <div class="form-group">
    <label for="exampleInputPassword1">Email</label>
    <input type="email" name="email" class="form-control"  placeholder="Email" value="<?php echo $email; ?>" required>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password"  name="password" class="form-control"  placeholder="Password" value="<?php echo $_POST['password']; ?>" required>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Confirm Password</label>
    <input type="password"  name="cpassword" class="form-control"  placeholder="Confirm Password" value="<?php echo $_POST['cpassword']; ?>" required>
  </div>
 
  <center>
  <button  name="submit" class="btn btn-outline-primary">Submit here</button>&nbsp &nbsp &nbsp 
  </center>
  <br>
  <p class="login-register-text">Have an account? <a href="dashboard.php">Login Here</a>.</p>
  </div>
</form>
</div>

    <!--image slider start-->
 
    <h3><i class="fas fa-bullhorn"></i> &nbsp Announcement</h3>
    <div class="slider">
      <div class="slides">
        <!--radio buttons start-->
        <input type="radio" name="radio-btn" id="radio1">
        <input type="radio" name="radio-btn" id="radio2">
        <input type="radio" name="radio-btn" id="radio3">
        <input type="radio" name="radio-btn" id="radio4">
        <!--radio buttons end-->
        <!--slide images start-->
        <div class="slide first">
          <img src="img/1.jpg" alt="">
        </div>
        <div class="slide">
          <img src="img/5.png" alt="">
        </div>
        <div class="slide">
          <img src="img/6.png" alt="">
        </div>
        <div class="slide">
          <img src="img/2.png" alt="">
        </div>
        <!--slide images end-->
        <!--automatic navigation start-->
        <div class="navigation-auto">
          <div class="auto-btn1"></div>
          <div class="auto-btn2"></div>
          <div class="auto-btn3"></div>
          <div class="auto-btn4"></div>
        </div>
        <!--automatic navigation end-->
      </div>
      <!--manual navigation start-->
      <div class="navigation-manual">
        <label for="radio1" class="manual-btn"></label>
        <label for="radio2" class="manual-btn"></label>
        <label for="radio3" class="manual-btn"></label>
        <label for="radio4" class="manual-btn"></label>
      </div>
      <!--manual navigation end-->
    </div>
    <!--image slider end-->
    
    <script type="text/javascript">
    var counter = 1;
    setInterval(function(){
      document.getElementById('radio' + counter).checked = true;
      counter++;
      if(counter > 4){
        counter = 1;
      }
    }, 5000);
    </script>

  </body>
</html>