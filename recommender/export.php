<?php  
//export.php  
include 'db.php';
$output = '';
if(isset($_POST["export"]))
{
 $query = "SELECT * FROM enrollment order by 1 desc";
 $result = mysqli_query($con, $query);
 if(mysqli_num_rows($result) > 0)
 {
  $output .= '
   <table class="table" bordered="1">  
                    <tr>  
                         <th>S.L</th>  
                         <th> Enrollees Name</th>  
                         <th>Gender</th>  
                         <th>Email</th>  
                         <th>Contact</th>  
                         <th>Student_id</th>  
                         <th>Degree</th>
                         <th>School_grad</th>
                         <th>Year_grad</th>  
                         <th>Academic_award</th>
                         <th>Takes</th>  
                         <th>Majorship</th>
                         <th>Payment</th>  
                         <th>Facebook Name</th>  
                         <th>Issue Date:</th>

                    </tr>
  ';
  $i = 0;
  while($row = mysqli_fetch_array($result))
  {
    $sl = ++$i;
   $output .= '
    <tr>  
                         <td > '.$sl.' </td>
                        
                         <td>'.$row["fname"]  .$row["lname"] .$row["mname"].'</td>  
                         <td>'.$row["gender"].'</td>  
                         <td>'.$row["email"].'</td>  
                         <td>'.$row["contact"].'</td>  
                         <td>'.$row["student_id"].'</td>  
                         <td>'.$row["degree"].'</td> 
                         <td>'.$row["school_grad"].'</td>  
                         <td>'.$row["year_grad"].'</td>  
                         <td>'.$row["academic_award"].'</td> 
                         <td>'.$row["first_take"].'</td>  
                         <td>'.$row["majorship"].'</td>  
                        <td>'.$row["pay"].'</td>  
                        <td>'.$row["fb_name"].'</td>  
                        <td>'.$row["uploaded"].'</td>
                    </tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Enrollees_Details.xls');
  echo $output;
 }
}
?>